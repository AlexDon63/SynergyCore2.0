package net.synergyserver.synergycore.database;

import com.mongodb.AggregationOptions;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import net.synergyserver.synergycore.MemberApplication;
import net.synergyserver.synergycore.SerializableLocation;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.profiles.DiscordProfile;
import net.synergyserver.synergycore.profiles.DubtrackProfile;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.SynUser;
import net.synergyserver.synergycore.profiles.WebsiteProfile;
import net.synergyserver.synergycore.profiles.WorldGroupProfile;
import net.synergyserver.synergycore.settings.SettingDiffs;
import net.synergyserver.synergycore.settings.SettingPreferences;
import net.synergyserver.synergycore.settings.SettingPreset;
import org.bukkit.configuration.file.YamlConfiguration;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import java.util.ArrayList;
import java.util.List;

/**
 * The class used to interface with the MongoDB database.
 */
public class MongoDB {

    private static MongoDB instance = null;
    private Morphia morphia;
    private Datastore datastore;

    /**
     * Creates a new <code>MongoDB</code> object and connects it to the database.
     */
    private MongoDB() {
        morphia = new Morphia();
    }

    /**
     * Returns the object representing this <code>MongoDB</code>.
     *
     * @return The object of this class.
     */
    public static MongoDB getInstance() {
        if (instance == null) {
            instance = new MongoDB();
        }
        return instance;
    }

    /**
     * Connects to the database and prepares classes.
     */
    public void connect() {
        // Point the object factory to the right class loader
        morphia.getMapper().getOptions().setObjectFactory(new HackyCreator());

        // Prepare the MongoClient
        YamlConfiguration config = SynergyCore.getPluginConfig();
        String ip = config.getString("database.address");
        int port = config.getInt("database.port");
        ServerAddress serverAddress = new ServerAddress(ip, port);

        List<MongoCredential> credentialList = new ArrayList<>();
        String username = config.getString("database.username");
        String dbname = config.getString("database.name");
        char[] password = config.getString("database.password").toCharArray();
        MongoCredential auth = MongoCredential.createCredential(username, dbname, password);
        credentialList.add(auth);

        MongoClient client = new MongoClient(serverAddress, credentialList);

        // Create a Datastore to hold the database's data
        datastore = morphia.createDatastore(client, dbname);
        datastore.ensureIndexes();
    }

    /**
     * Gets the <code>Datastore</code> object used to interface with the database.
     *
     * @return The <code>Datastore</code> object used to interface with the database.
     */
    public Datastore getDatastore() {
        return datastore;
    }

    /**
     * Passes the given classes on to Morphia for object mapping for being saved
     * and retrieved to/from the database. All classes whose objects are to be
     * stored in the database in a non-binary data format must be mapped.
     *
     * @param classes The classes to map.
     */
    public void mapClasses(Class... classes) {
        morphia.map(classes);
    }

    /**
     * This calls the map method on Morphia to prepare data classes for being saved
     * and retrieved to/from the database. All classes whose objects are to be
     * stored in the database in a non-binary data format must be mapped.
     */
    private void mapObjects() {
        // Map main DataEntities
        morphia.map(
                SynUser.class,
                DiscordProfile.class,
                DubtrackProfile.class,
                MinecraftProfile.class,
                WebsiteProfile.class,
                WorldGroupProfile.class,
                SettingPreferences.class,
                MemberApplication.class
        );
        // Also map embedded classes
        morphia.map(
                SerializableLocation.class,
                SettingDiffs.class,
                SettingPreset.class
        );
    }

    /**
     * Used as a temporary fix to MongoDB 3.6's new syntax for aggregation.
     * To be replaced with a less ghetto method in the future.
     */
    public static AggregationOptions getAggregationOptions() {
        return AggregationOptions.builder().outputMode(AggregationOptions.OutputMode.CURSOR).build();
    }
}
