package net.synergyserver.synergycore.database;

import org.mongodb.morphia.logging.Logger;

public class HackyLogger implements Logger {

    public void debug(String s) {

    }

    public void debug(String s, Object... objects) {

    }

    public void debug(String s, Throwable throwable) {

    }

    public void error(String s) {

    }

    public void error(String s, Object... objects) {

    }

    public void error(String s, Throwable throwable) {

    }

    public void info(String s) {

    }

    public void info(String s, Object... objects) {

    }

    public void info(String s, Throwable throwable) {

    }

    public boolean isDebugEnabled() {
        return false;
    }

    public boolean isErrorEnabled() {
        return false;
    }

    public boolean isInfoEnabled() {
        return false;
    }

    public boolean isTraceEnabled() {
        return false;
    }

    public boolean isWarningEnabled() {
        return false;
    }

    public void trace(String s) {

    }

    public void trace(String s, Object... objects) {

    }

    public void trace(String s, Throwable throwable) {

    }

    public void warning(String s) {

    }

    public void warning(String s, Object... objects) {

    }

    public void warning(String s, Throwable throwable) {

    }
}
