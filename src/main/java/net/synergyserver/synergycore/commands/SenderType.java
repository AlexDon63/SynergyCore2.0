package net.synergyserver.synergycore.commands;

import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

/**
 * Types of command senders that are handled by SynergyCore.
 */
public enum SenderType {
    PLAYER,
    CONSOLE,
    BLOCK,
    UNKNOWN;

    /**
     * Gets the <code>SenderType</code> of the given CommandSender.
     *
     * @param sender The CommandSender to get check.
     * @return The <code>SenderType</code> of the CommandSender.
     */
    public static SenderType getSenderType(CommandSender sender) {
        SenderType senderType = UNKNOWN;

        if (sender instanceof Player) {
            senderType = PLAYER;
        } else if (sender instanceof ConsoleCommandSender) {
            senderType = CONSOLE;
        } else if (sender instanceof BlockCommandSender) {
            senderType = BLOCK;
        }

        return senderType;
    }
}
