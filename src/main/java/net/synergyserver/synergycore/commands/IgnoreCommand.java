package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

@CommandDeclaration(
        commandName = "ignore",
        aliases = "block",
        permission = "syn.ignore",
        usage = "/ignore <player>",
        description = "Ignores the specified player, blocking their messages and requests to you.",
        minArgs = 1,
        maxArgs = 1,
        validSenders = SenderType.PLAYER
)
public class IgnoreCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        UUID toIgnore = PlayerUtil.getUUID(args[0], true, player.hasPermission("vanish.see"));

        // If no player was found then give the sender an error message
        if (toIgnore == null) {
            player.sendMessage(Message.format("commands.error.player_not_found", args[0]));
            return false;
        }

        // If they're trying to ignore themselves then give the sender an error message
        if (toIgnore.equals(player.getUniqueId())) {
            player.sendMessage(Message.get("commands.ignore.error.self"));
            return false;
        }

        String toIgnoreName = PlayerUtil.getName(toIgnore);

        // If the specified player cannot be ignored then give the sender an error message
        if (PlayerUtil.hasGlobalPermission(toIgnore, "syn.ignore.exempt")) {
            player.sendMessage(Message.format("commands.ignore.error.exempt", toIgnoreName));
            return false;
        }

        MinecraftProfile mcp = PlayerUtil.getProfile(player);

        // If they're already ignoring the player then give the sender an error message
        if (mcp.hasIgnored(toIgnore)) {
            player.sendMessage(Message.format("commands.ignore.error.already_ignored", toIgnoreName));
            return false;
        }

        // Ignore the player and give feedback
        mcp.addIgnoredPlayer(toIgnore);
        player.sendMessage(Message.format("commands.ignore.info.ignore_success", toIgnoreName));
        return true;
    }
}
