package net.synergyserver.synergycore.commands;

import org.bukkit.command.CommandSender;

@CommandDeclaration(
        commandName = "test",
        aliases = {"ayy", "lmao"},
        permission = "syn.test",
        usage = "/test foo",
        description = "This is a debug thing"
)
public class TestCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {

        for (String flag : flags.getFlags().keySet()) {
            sender.sendMessage(flag + ": " + flags.getFlags().get(flag));
        }
        return true;
    }
}
