package net.synergyserver.synergycore.commands;


import net.synergyserver.synergycore.configs.Message;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

@CommandDeclaration(
        commandName = "Hat",
        aliases = {"helmet", "wear"},
        permission = "syn.hat",
        usage = "/hat",
        description = "Puts the currently held item on your head, no matter how silly it would be!",
        maxArgs = 0,
        validSenders = SenderType.PLAYER
)
public class HatCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags){
        Player player = (Player) sender;
        PlayerInventory inventory = player.getInventory();

        ItemStack mainHand = inventory.getItemInMainHand();
        ItemStack oldHat = inventory.getHelmet();

        if (mainHand == null || mainHand.getType().equals(Material.AIR)){
            // If they're wearing a hat, give it back. Otherwise give an error
            if (oldHat != null && !oldHat.getType().equals(Material.AIR)) {
                inventory.setHelmet(null);
                inventory.setItemInMainHand(oldHat);
                player.sendMessage(Message.format("commands.hat.info.old_hat_returned", oldHat.getType().name()));
                return true;
            } else {
                player.sendMessage(Message.format("commands.hat.error.no_held_item"));
                return false;
            }
        } else if (oldHat != null && mainHand.getType().equals(oldHat.getType())){
            // Give a special message if the item they're holding is the same as their existing hat
            player.sendMessage(Message.format("commands.hat.info.already_wearing", mainHand.getType().name()));
            return true;
        } else {
            // Replace their hat with the currently held item
            // Create the new hat
            ItemStack newHat = mainHand.clone();
            newHat.setAmount(1);
            mainHand.setAmount(mainHand.getAmount() - 1);
            inventory.setHelmet(newHat);

            // Give back the player's old hat if it exists
            if (oldHat != null) {
                inventory.addItem(oldHat);
            }

            // Give feedback
            player.sendMessage(Message.format("commands.hat.info.enjoy_hat", newHat.getType().name()));
            return true;
        }
    }
}