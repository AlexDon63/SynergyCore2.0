package net.synergyserver.synergycore.commands;

import org.bukkit.command.CommandSender;

@CommandDeclaration(
        commandName = "serviceunban",
        aliases = "sunban",
        permission = "syn.serviceban",
        usage = "/serviceunban <discord|dubtrack>",
        description = "Main command for unbanning players from a service.",
        minArgs = 2,
        validSenders = {SenderType.PLAYER, SenderType.CONSOLE}
)
public class ServiceUnbanCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        return false;
    }
}
