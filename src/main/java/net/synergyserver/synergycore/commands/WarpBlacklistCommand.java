package net.synergyserver.synergycore.commands;

import org.bukkit.command.CommandSender;

@CommandDeclaration(
        commandName = "blacklist",
        aliases = "deny",
        permission = "syn.warp.blacklist",
        usage = "/warp blacklist <add|remove>",
        description = "Warp subcommand for managing blacklists.",
        minArgs = 1,
        validSenders = SenderType.PLAYER,
        parentCommandName = "warp"
)
public class WarpBlacklistCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        return false;
    }
}
