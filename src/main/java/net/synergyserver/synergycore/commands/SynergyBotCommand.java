package net.synergyserver.synergycore.commands;

import org.bukkit.command.CommandSender;

@CommandDeclaration(
        commandName = "synergybot",
        aliases = {"synergybots", "sbot", "sbots", "bot", "bots"},
        permission = "syn.synergybot",
        usage = "/synergybot <login|logout>",
        description = "Main command for making the service bots perform actions."
)
public class SynergyBotCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        return false;
    }
}
