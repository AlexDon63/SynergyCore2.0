package net.synergyserver.synergycore.listeners;

import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.events.SettingChangeEvent;
import net.synergyserver.synergycore.guis.GUI;
import net.synergyserver.synergycore.guis.GUIInventoryHolder;
import net.synergyserver.synergycore.guis.GUIView;
import net.synergyserver.synergycore.guis.Itemizable;
import net.synergyserver.synergycore.guis.SettingGUI;
import net.synergyserver.synergycore.profiles.WorldGroupProfile;
import net.synergyserver.synergycore.settings.FixedOptionsSetting;
import net.synergyserver.synergycore.settings.Setting;
import net.synergyserver.synergycore.settings.SettingCategory;
import net.synergyserver.synergycore.settings.SettingDiffs;
import net.synergyserver.synergycore.settings.SettingManager;
import net.synergyserver.synergycore.settings.SettingPreferences;
import net.synergyserver.synergycore.settings.SettingPreset;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

/**
 * Listens to interact events on <code>GUI</code>s.
 */
public class GUIListener implements Listener {

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onGUIInteract(InventoryClickEvent event) {
        if (event.getInventory().getHolder() instanceof GUIInventoryHolder) {
            // Cancel the event so that the item cannot be stolen from the GUI
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onSettingGUIInteract(InventoryClickEvent event) {
        Inventory topInventory = event.getInventory();

        // Ignore the event if it isn't for a GUI
        if (!(topInventory.getHolder() instanceof GUIInventoryHolder)) {
            return;
        }

        // Ignore the event if it isn't for a SettingGUI
        GUIInventoryHolder guiInventoryHolder = (GUIInventoryHolder) topInventory.getHolder();
        GUI gui = guiInventoryHolder.getGui();
        if (!(gui instanceof SettingGUI)) {
            return;
        }

        // Ignore the event if it wasn't the gui that was clicked
        if (event.getClickedInventory() == null || !event.getClickedInventory().equals(topInventory)) {
            return;
        }

        // Ignore the event if the slot clicked wasn't an item
        GUIView view = guiInventoryHolder.getView();
        Itemizable itemizable = view.getItemizable(event.getSlot());
        if (itemizable == null) {
            return;
        }

        // Ignore the event if, for whatever reason, the player who clicked isn't a player
        if (!(event.getWhoClicked() instanceof Player)) {
            return;
        }

        Player player = (Player) event.getWhoClicked();
        SettingGUI settingGUI = (SettingGUI) gui;
        SettingPreferences sp = settingGUI.getSettingPreferences();

        if (itemizable instanceof SettingPreset) {
            // If the user is interacting with a preset
            SettingPreset preset = (SettingPreset) itemizable;

            // Set each of the changed settings
            SettingManager sm = SettingManager.getInstance();
            for (String key : preset.getDiffs().keySet()) {
                Setting setting = sm.getSettings().get(key);
                Object oldValue = setting.getValue(sp.getCurrentSettings(), player);
                Object newValue = setting.getValue(preset, player);

                SettingChangeEvent settingChangeEvent = new SettingChangeEvent(player, setting, oldValue, newValue);
                Bukkit.getPluginManager().callEvent(settingChangeEvent);

                // Check if the event was cancelled before continuing
                if (settingChangeEvent.isCancelled()) {
                    return;
                }

                // Set the new value of the changed setting
                sp.setCurrentSetting(setting.getIdentifier(), newValue);
            }

            // Update the GUI and display it
            WorldGroupProfile wgp = PlayerUtil.getProfile(player).getCurrentWorldGroupProfile();
            wgp.createSettingGUI();
            SettingGUI newGUI = wgp.getSettingGUI();
            // Apparently we're not supposed to call openInventory by this EventHandler
            Bukkit.getScheduler().runTask(SynergyCore.getPlugin(), () -> player.openInventory(newGUI.getView().getInventory()));
        } else if (itemizable instanceof FixedOptionsSetting) {
            // If the user is trying to change a setting
            FixedOptionsSetting fixedOptionsSetting = (FixedOptionsSetting) itemizable;
            SettingDiffs currentSettings = sp.getCurrentSettings();

            // Get the next value of the setting that the player has access to
            Object currentValue = fixedOptionsSetting.getValue(currentSettings, player);
            Object nextValue = fixedOptionsSetting.getNextValue(currentValue);
            while (!player.hasPermission(fixedOptionsSetting.getOption(nextValue).getPermission())) {
                // Break out of an infinite loop if the current value equals nextValue
                if (currentValue.equals(nextValue)) {
                    break;
                }
                // Try the next value
                nextValue = fixedOptionsSetting.getNextValue(nextValue);
            }

            // Emit an event
            SettingChangeEvent settingEvent = new SettingChangeEvent(player, fixedOptionsSetting, currentValue, nextValue);
            Bukkit.getPluginManager().callEvent(settingEvent);

            // Check if the event was cancelled before continuing
            if (settingEvent.isCancelled()) {
                return;
            }

            // Set the new value of the clicked setting
            sp.setCurrentSetting(fixedOptionsSetting.getIdentifier(), nextValue);

            // Update the view and the inventory that the player sees
            view.setItemizable(fixedOptionsSetting, nextValue, event.getSlot());
            player.updateInventory();
        } else if (itemizable instanceof SettingCategory) {
            // If the user is switching categories
            SettingCategory category = (SettingCategory) itemizable;

            // Apparently we're not supposed to call openInventory by this EventHandler
            Bukkit.getScheduler().runTask(SynergyCore.getPlugin(), () -> player.openInventory(settingGUI.getGUIView(category).getInventory()));
        }
    }

}
