package net.synergyserver.synergycore;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.PlayerUtil;

import java.util.UUID;

/**
 * Represents a private message.
 */
public class PrivateMessage {

    private UUID sender;
    private UUID receiver;
    private long timeSent;
    private String message;
    private PrivateMessageType type;

    /**
     * Required constructor for Morphia to work.
     */
    protected PrivateMessage() {}

    /**
     * Creates a new <code>PrivateMessage</code> with the given parameters.
     *
     * @param sender The UUID of the sender.
     * @param receiver The UUID of the receiver.
     * @param timeSent The time this message was sent, as milliseconds since the epoch.
     * @param message The message itself.
     * @param type The <code>PrivateMessageType</code> of the message.
     */
    public PrivateMessage(UUID sender, UUID receiver, long timeSent, String message, PrivateMessageType type) {
        this.sender = sender;
        this.receiver = receiver;
        this.timeSent = timeSent;
        this.message = message;
        this.type = type;
    }

    /**
     * Gets the UUID of the sender of this message.
     *
     * @return The UUID of the sender.
     */
    public UUID getSender() {
        return sender;
    }

    /**
     * Gets the UUID of the receiver of this message.
     *
     * @return The UUID of the receiver.
     */
    public UUID getReceiver() {
        return receiver;
    }

    /**
     * Sets the UUID of the receiver of this message.
     *
     * @param receiver The UUID of the receiver.
     */
    public void setReceiver(UUID receiver) {
        this.receiver = receiver;
    }

    /**
     * Gets the time when this message was sent as milliseconds since the unix epoch.
     *
     * @return The time when this message was sent.
     */
    public long getTimeSent() {
        return timeSent;
    }

    /**
     * Gets the message of this message.
     *
     * @return The message of this message.
     */
    public String getMessage() {
        return message;
    }

    /**
     * Gets the <code>PrivateMessageType</code> of this message.
     *
     * @return The type of this message.
     */
    public PrivateMessageType getType() {
        return type;
    }

    /**
     * Checks if this message is designated as being official.
     *
     * @return True if this message is official.
     */
    public boolean isOfficial() {
        return type.equals(PrivateMessageType.OFFICIAL);
    }

    /**
     * Gets the name of the sender of this message, taking into account the possibility of official messages.
     *
     * @return The name of the sender of this message.
     */
    public String getSenderName() {
        return isOfficial() ? Message.get("mail.staff_sender") : PlayerUtil.getName(sender);
    }
}
